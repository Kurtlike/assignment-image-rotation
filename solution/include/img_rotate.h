#ifndef IMG_ROTATE_H
#define IMG_ROTATE_H
#include "bmp.h"
#include "image.h"
#include <stdio.h>

int img_rotate( char* in, char* out);

#endif
