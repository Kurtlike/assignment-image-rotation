#ifndef BMP_H
#define BMP_H
#include "image.h"
#include <stdio.h>
#include <stdlib.h>

enum read_status {
    READ_OK = 0,
    READ_ERROR = -1
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR = -1
};

enum read_status from_bmp(FILE* in, struct image* image);
enum write_status to_bmp(FILE* out, struct image const* image);

#endif //BMP_H
