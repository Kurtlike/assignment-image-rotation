#ifndef ROTATE_H
#define ROTATE_H
#include <stdio.h>
#include <stdint.h>
struct __attribute__((packed)) pixel {
    uint8_t blue, green, red;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image rotate(struct image const* source );
void free_image(struct image* image);
#endif //ROTATE_H
