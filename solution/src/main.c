#include "../include/img_rotate.h"
#include <stdio.h>

int main(int argc, char **argv) {
    (void) argc;
    (void) argv; // supress 'unused parameters' warning

    
    char * input_path = argv[1];
    char * output_path = argv[2];
    return img_rotate(input_path, output_path);
}
