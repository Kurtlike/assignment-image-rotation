#include "../include/img_rotate.h"
int img_rotate(char* in, char* out){
	int error = 1;
	struct image image = {0};
    FILE *bmp_in = fopen(in, "rb");
    if(from_bmp(bmp_in, &image) != READ_ERROR) error = 0;
    fclose(bmp_in);

    struct image image_rotated = rotate(&image);
    free_image(&image);

    FILE *bmp_out = fopen(out, "wb");
    if(to_bmp(bmp_out, &image_rotated) == WRITE_ERROR) error = 1;
    fclose(bmp_out);

    free_image(&image_rotated);
    return error;
}
