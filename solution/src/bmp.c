#include "bmp.h"

#pragma pack(push, 1) // выравниваем структуру, чтоб потом можно было спокойно считать ее напрямую с файла
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

static struct bmp_header bmp_header_set(const struct image* image) {
   struct bmp_header bmpHeader = {
           .bfType = 0x4d42,
           .bfileSize = image->height * image->width * sizeof(struct pixel) +
                        image->height * (image->width % 4) + sizeof(struct bmp_header),
           .bfReserved = 0,
           .bOffBits = sizeof(struct bmp_header),
           .biSize = 40,
           .biWidth = image->width,
           .biHeight = image->height,
           .biPlanes = 1,
           .biBitCount = 24,
           .biCompression = 0,
           .biSizeImage =  image->height * image->width * sizeof(struct pixel) + (image->width % 4)*image->height,
           .biXPelsPerMeter = 0,
           .biYPelsPerMeter = 0,
           .biClrUsed = 0,
           .biClrImportant = 0,
   };
    return bmpHeader;
}

enum read_status from_bmp(FILE* in, struct image* image) {
    if(in == NULL) return READ_ERROR;
    struct bmp_header bmp_header = {0};
    if(fread(&bmp_header, 1, sizeof(struct bmp_header), in) !=sizeof(struct bmp_header)) return READ_ERROR;
    image->height = bmp_header.biHeight;
    image->width = bmp_header.biWidth;
    image->data = (struct pixel*) malloc(image->width * image->height * sizeof(struct pixel));

    for (size_t i = 0; i < image->height; i++) {
        if(fread(&(image->data[i*image->width]), sizeof(struct pixel), image->width, in) != image->width || fseek(in, (uint8_t)image->width  % 4, SEEK_CUR)){
            free_image(image);
            return READ_ERROR;
        } 
        
    }

    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* image) {
    if(out == NULL) return WRITE_ERROR;
    struct bmp_header bmp_header = bmp_header_set(image);

    bmp_header.biHeight = image->height;
    bmp_header.biWidth = image->width;

    if(fwrite(&bmp_header, sizeof(struct bmp_header), 1, out) != 1) return WRITE_ERROR;
    const uint64_t zero = 0;
    for(size_t i = 0; i < image->height; i++) {
        if(fwrite(&(image->data[i * image->width]), sizeof(struct pixel), image->width, out) !=image->width || fwrite(&zero, 1, image->width % 4, out) != image->width % 4 ){
            return WRITE_ERROR;
        }
          
    }

    return WRITE_OK;
    
}
